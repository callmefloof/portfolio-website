---
title: Learning the basics of Live2D
date: 2022-10-17T14:30:44.794Z
---
As an exercise in character design and rigging in [Live2D Cubism](https://www.live2d.com/en/) I decided to commission an artist to make my own Live2D avatar.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LalV4RawrY8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The artwork used was made by [@creampuffmochi](https://twitter.com/creampuffmochi).
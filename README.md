# FloofDigitalMedia

The the codebase of this project is licensed under either MIT or LGPLv3.
A list of exceptions is provided below, along with the license for each one. When using these assets, please be mindful that you are using the correct license.

The following are licenced under CC-BY-SA:
- android-chrome-192x192.png
- android-chrome-512x512.png
- apple-touch-icon.png
- favicon-16x16.png
- favicon-32x32.png
- favicon.ico



## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

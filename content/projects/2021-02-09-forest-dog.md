---
title: Forest Dog
date: 2021-02-09T13:59:29.369Z
description: A game I participated in creating during the Global Game Jam.
img: https://img.itch.zone/aW1hZ2UvOTAyOTMwLzUwOTkxMTQuanBn/original/82jtEA.jpg
---
During the GGJ I together with Nathan BonKerk, lazycatdev, NoMercyDay, and InfamyKing made a puzzle adventure game called [Forest Dog](https://nathan-bonkerk.itch.io/forest-dog).

It's a game about an elderly individual overcoming their dementia and finding back the memories of their beloved dog.

I was responsible for implementing mechanics like the fog and brainstroming ideas.

The game also has a [WebGL version](https://callmefloof.itch.io/forestdog-webgl-version) which is playable in the browser, which I built and submitted after the Windows version.

### Images:

<div id="images0">
<b-img fluid src="https://img.itch.zone/aW1hZ2UvOTAyOTMwLzUwOTkxMTUuanBn/original/U1Btma.jpg" style="max-width:50%"></b-img>

<b-img fluid src="https://img.itch.zone/aW1hZ2UvOTAyOTMwLzUwOTkxMTcuanBn/original/NncX4h.jpg" style="max-width:50%"></b-img>

<b-img fluid src="https://img.itch.zone/aW1hZ2UvOTAyOTMwLzUwOTkxMTQuanBn/original/82jtEA.jpg" style="max-width:50%"/></b-img>
</div>
<style>
    #images0{
        display: grid;
        max-width: 90%;
        margin: auto;
    }
    #images0 > *{
        margin: auto;
        margin-top: 1em;
        margin-bottom: 1em;
        max-width:50%;
    }
</style>

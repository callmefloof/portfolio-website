---
title: contact
date: 2021-02-09T13:37:22.095Z
description: contact details
---
## Contact details:

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<i class="fa fa-envelope" style="font-size:2.1em"></i><a href="mailto:j.l.ubink@floofdm.com"> j.l.ubink@floofdm.com</a>
<br/>
<br/>
<svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
  <title>LinkedIn</title>
    <g>
      <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" fill="currentColor"></path>
    </g>
</svg><a href="https://www.linkedin.com/in/joëlle-ubink-561156155"> LinkedIn</a>

<style>
  .nuxt-content > h2{
    margin-left: 1em !important;
  }
  .nuxt-content > *{
    margin:0 !important;
  }
  .nuxt-content > i{
    margin-top: 0.2em !important;
    margin-left: 2em !important;
    margin-right: 0.2em !important;
  }
  .nuxt-content > svg{
    margin-left: 4.2em !important;
    margin-right: 0.2em !important;
    margin-bottom: 1em !important;
    transform: scaleX(1.05) scaleY(1.05);
  }
</style>
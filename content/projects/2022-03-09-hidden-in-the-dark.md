---
title: Hidden in the Dark
date: 2022-03-09T21:36:50.431Z
img: https://img.itch.zone/aW1nLzgwMTUzMTcucG5n/347x500/6p8GhU.png
---
During this year's global game jam I worked on a game called Hidden in the Dark. The creation was a collaboration between me and my fellow jammers. I was responsible for helping to create a concept, create a level based on a level layout provided to me, organize the project through trello, building, and submitting the game for the deadline.

This game has been a step up from last year. I got a lot more aqquanted with Unreal Engine's workflow and scripting. It also stressed to me how crucial it is to have a composer in your team to work out sound design from the very beginning.

A link to the game can be found [here](https://callmefloof.itch.io/hidden-in-the-dark).
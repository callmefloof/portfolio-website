---
title: TFIFT
date: 2021-05-12T15:33:19.777Z
description: A cyberpunk concept.
---
<br>

TFIFT (w.i.p. name) is a cyberpunk setting focused on how a society could function in an isolated environment where innovation, progress and growth are both the law and its core driving force at any and all costs. It was my first time documenting a concept.

A link to its one-pager can be found <a href="https://floofdm.com/img/tfift.pdf">here.</a>

<br/>

**update 25-09-2022**



During the spring of this year, I made a new design document based on the original one-pager with a new (w.i.p.) title.

This new title is: "Noyager".

A link to the new document can be found <a href="https://drive.google.com/file/d/1av2013wCRDGh-hzdB1NPJrvMQ6XQvddk/view?usp=sharing">here.</a>

<br/>


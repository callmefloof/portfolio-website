---
title: A peek behind this site's code
date: 2022-01-05T15:22:46.683Z
---

<br/>

I figured I would share the code of my site and some of the reasoning behind the design.

The core goal was to create something visually flat and basic for visual clarity while still incorporating some personality in the aesthetics.

The site itself was built with Vue.JS and Nuxt for static site generation. Bootstrap was used for templating alongside Font-Awesome for a few additional icons.
In spite of the use of frameworks. I wanted the site to be as functional as possible without the use of JavaScript. In order to achieve this I extensively relied on CSS for interactivity and animations.
The theming is dark in part because of personal taste and my photosensitivity making it hard to look at white backgrounds for extended periods of time.
The main background was something I was quite proud of achieving in spite of its simplicity. The shifting colors were achieved by using a combination of SVG radial gradients and CSS to animate it.





The code can be found [here](https://gitlab.com/callmefloof/portfolio-website).

<br/>
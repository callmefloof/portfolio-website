---
title: index
date: 2021-05-12T13:49:34.732Z
description: About me
---
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    input:checked + .hide, h4{
        opacity: 1;
        display: block;
        transition: opacity 0.5s linear;
    }
    .hide{
        opacity: 0;
        display: none;
    }

</style>
<br/>

# <p class="text-flicker-in-glow">About me</p>

<br/>
<b-img fluid center  src="/android-chrome-512x512.png" style="max-width:40%; position"></b-img>
<br/>

Hello,
I'm Joëlle Ubink, also known as *"Floof"* online.

I'm a Game Design Student from the Netherlands with a background in Software Engineering. I'm fond of managing people and projects creating systems, observing how they interlink with one another, and have a wide range of other interests. 
Examples of these interests would be: World Politics, (Modern) History, Computer Hardware, Psychology and Psychiatry.

Furthermore, I've got a strong passion for Games and their ability for carrying narrative through gameplay as well as being able to invoke emotions in an interactive manner, especially the role-playing genre for providing agency, freedom of choice, and immersion for players.

A link to my resume can be found [here](https://drive.google.com/file/d/1PPbvnqAzlZn0QBVAbx2Q6_EB4cJVSZPO/view?usp=sharing).

#### Skills: <label for=toggleSkills class="fa fa-caret-down" aria-hidden="true"></label>

<input type='checkbox' style='display:none' id=toggleSkills>
<ul class="hide" id="Skills">
    <li>
        Production <label for="toggleProduction" href="#toggleProduction" class="fa fa-caret-down" aria-hidden="true"></label>
        <input type='checkbox' style='visibility:hidden' id=toggleProduction>
        <ul class="hide" id="toggleProduction">
            <li>Agile</li>
            <li>SCRUM</li>
            <li>Waterfall</li>
            <li>Software <label for="toggleProductionSoftware" href="#toggleProductionSoftware" class="fa fa-caret-down" aria-hidden="true"></label>
                <input type='checkbox' style='visibility:hidden' id=toggleProductionSoftware>
            <ul class="hide" id="ProductionSoftware">
                <li>Trello</li>
            </ul>
            </li>
        </ul>
    </li>
    <li>
        Design <label for="toggleDesign" href="#Design" class="fa fa-caret-down" aria-hidden="true"></label>
        <input type='checkbox' style='visibility:hidden' id=toggleDesign>
        <ul class="hide" id="Design">
            <li>Technical Writing</li>
            <li>Creative Writing</li>
            <li>UI/UX</li>
            <li>Photoshop</li>
            <li>Illustrator</li>
            <li>Premiere Pro</li>
        </ul>
    </li>
    <li>Game Engines <label class="fa fa-caret-down" for="toggleEngines" aria-hidden="true"></label>
        <input type='checkbox' style='visibility:hidden' id=toggleEngines>
        <ul class="hide" id="Engines">
            <li>Unity</li>
            <li>Game Maker</li>
            <li>Unreal Engine 4</li>
        </ul>
    </li>
    <li>Programming <label for="toggleProgramming" href="#toggleProgramming" class="fa fa-caret-down" aria-hidden="true"></label>
        <input type='checkbox' style='visibility:hidden' id=toggleProgramming>
        <ul class="hide" id="Programming">
            <li>C#</li>
            <li>GML</li>
            <li>C/C++</li>
            <li>Web <label for="toggleWeb" href="#toggleWeb" class="fa fa-caret-down" aria-hidden="true"></label>
                <input type='checkbox' style='visibility:hidden' id=toggleWeb>
                <ul class= "hide" id="Web">
                    <li>HTML/CSS/Javascript</li>
                    <li>Vue.js/Nuxt</li>
                    <li>JQuery</li>
                </ul>
            </li>
    </li>
        </ul>
    </li>
    <li>Languages <label for="toggleLanguage" href="#toggleLanguage" class="fa fa-caret-down" aria-hidden="true"></label>
        <input type='checkbox' style='visibility:hidden' id=toggleLanguage>
        <ul class="hide" id="toggleLanguage">
            <li>English</li>
            <li>Dutch</li>
            <li>Basic Knowledge of: 
                <ul>
                    <li>Japanese</li>
                    <li>German</li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

#### Favorite Games: <label class="fa fa-caret-down" for="toggleGames" aria-hidden="true"></label>

<input type='checkbox' style='visibility:hidden' id=toggleGames>
<ul class="hide" id="Games">
    <li>Fallout New Vegas</li>
    <li>Persona 5 Royal</li>
    <li>Grand Theft Auto San Andreas</li>
    <li>The <i>Dark Souls</i> Series</li>
    <li>VA-11 Hall-A</li>
    <li>Euphoria</li>
</ul>
<br/>

import * as path from 'path';
import * as fs from 'fs';
export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Floof Digital Media',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'apple-touch-icon', type: 'image/png', sizes: '180x180', href: '/apple-touch-icon.png' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxt/content'
  ],

  

  content:{
    liveEdit: false
  },

  // generate: {
  //   async routes() {
  //     const contentPaths = ['blog'];
  //     const { $content } = require('@nuxt/content');
  //     const files = [];
  //     contentPaths.forEach(async (path) => {
  //       const file = await $content(path).fetch();
  //       files.push(file);
  //     });

  //     const generated = files.map((file) => {
  //       return {
  //         route: file.path === '/index' ? '/' : file.path,
  //         payload: fs.readFileSync(`./content/${file.path}${file.extension}`, 'utf-8'),
  //       };
  //     });

  //     return generated;
  //   },
  // },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    html: {
      minify:{
          collapseBooleanAttributes: true,
          decodeEntities: true,
          minifyCSS: false,
          minifyJS: false,
          processConditionalComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          trimCustomFragments: true,
          useShortDoctype: true,
          removeComments: true,
          preserveLineBreaks: true,
          collapseWhitespace: true
      }
  },

    }
}

---
title: "FGO: An Ethical Redesign"
date: 2022-01-06T02:05:54.243Z
---
As part of an assignment last year from February till April I redesigned the monetization model of Fate Grand Order. A link to the document can be found [here](https://drive.google.com/file/d/1ML5hHYR6HQPQEyF_1VspuQDlZgV2PoaH/view?usp=sharing).